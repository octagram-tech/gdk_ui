/***
 * Octagram GUI Crate
 * =================================
 * This crate is used for helping develop in-game graphical
 * user interfaces
 */

use macroquad::prelude::*;
use macroquad::ui::*;

static mut UI_WINDOWS: Option<Vec<Window>> = None;
static mut SCREEN_OPACITY: Option<f32> = None;

pub fn get_ui_windows() -> &'static mut Vec<Window> {
    unsafe {
        if UI_WINDOWS.is_none() {
            return UI_WINDOWS.get_or_insert(Vec::new());
        } else {
            return UI_WINDOWS
                .as_mut()
                .expect("Error: unable to get valid window instance!");
        }
    }
}

pub fn set_screen_opacity(value: f32) {
    *get_screen_rect_opacity() = value;
}

pub fn get_screen_rect_opacity() -> &'static mut f32 {
    unsafe {
        if SCREEN_OPACITY.is_none() {
            return SCREEN_OPACITY.get_or_insert(0.);
        } else {
            return SCREEN_OPACITY.as_mut().unwrap();
        }
    }
}

pub fn get_ui_window(id: &'static str) -> Option<&'static mut Window> {
    let windows = get_ui_windows();

    for i in 0..windows.len() {
        if windows[i].id == id {
            return Some(&mut windows[i]);
        }
    }

    None
}

pub fn get_ui_window_children(id: &'static str) -> &'static mut Vec<Box<dyn IComponent>> {
    let window = get_ui_window(id).unwrap_or_else(|| panic!("failed to get window!"));
    &mut window.children
}

pub fn add_component(window_id: &'static str, component: Component) {
    let extracted_window =
        get_ui_window(window_id).unwrap_or_else(|| panic!("failed to get window!"));
    extracted_window.add_child(component);
}

#[derive(Copy, Clone, PartialEq, Debug)]
pub enum DraggingState {
    Dragging,
    Dropped,
    None
}

pub type Component = Box<dyn IComponent>;

pub trait IComponent {
    fn add_child(&mut self, _component: Component) {}

    fn get_drag_state(&self) -> DraggingState {
        DraggingState::None
    }

    fn get_position(&self) -> Vec2 {
        Vec2::new(0., 0.)
    }

    fn get_drag_position(&self) -> Vec2 {
        Vec2::new(0., 0.)
    }

    fn get_opacity(&self) -> f32 {
        0.
    }

    fn set_position(&mut self, _value: Vec2) {}
    fn set_size(&mut self, _value: Vec2) {}
    fn set_opacity(&mut self, _value: f32) {}
    fn set_text(&mut self, _value: String) {}
    fn set_colour(&mut self, _value: Color) {}
    fn set_visible(&mut self, _visible: bool) {}

    fn is_clicked(&self) -> bool {
        return false;
    }

    fn is_visible(&self) -> bool;

    fn update(&mut self, ui_root: &mut Ui);
}

pub struct GroupComponent {
    pub group_skin: Skin,
    pub config: UIConfig,
    pub children: Vec<Box<dyn IComponent>>,
    pub draggable: bool,
    pub colour: Color,
    pub group_index: Option<usize>,
    pub drag_state: DraggingState,
    drag_position: Vec2
}

impl IComponent for GroupComponent {
    fn add_child(&mut self, component: Component) {
        self.children.push(component);
    }

    fn get_drag_state(&self) -> DraggingState {
        return self.drag_state;
    }

    fn get_drag_position(&self) -> Vec2 {
        self.drag_position
    }

    fn get_position(&self) -> Vec2 {
        self.config.pos
    }

    fn set_position(&mut self, value: Vec2) {
        self.config.pos = value;
    }

    fn set_size(&mut self, value: Vec2) {
        self.config.size = value;
    }

    fn set_opacity(&mut self, value: f32) {
        self.config.opacity = value;
    }

    fn set_colour(&mut self, value: Color) {
        self.colour = value;
    }

    fn set_visible(&mut self, visible: bool) {
        self.config.visible = visible;
    }

    fn is_visible(&self) -> bool {
        return self.config.visible;
    }

    fn update(&mut self, ui_root: &mut Ui) {
        ui_root.push_skin(&self.group_skin);

        let drag = widgets::Group::new(hash!("grp", self.group_index.unwrap()), self.config.size)
            .position(self.config.pos)
            .draggable(self.draggable)
            .ui(ui_root, |ui| {
                for child in &mut self.children {
                    child.update(ui);
                }
            });

        match drag {
            Drag::Dropped(_, _) => {
                self.drag_state = DraggingState::Dropped;
            }
            Drag::Dragging(pos, _) => {
                self.drag_position = pos;
                self.drag_state = DraggingState::Dragging;
            }
            Drag::No => {}
        }

        ui_root.pop_skin();
    }
}

pub struct Window {
    pub id: &'static str,
    pub window_skin: Skin,
    pub config: UIConfig,
    pub children: Vec<Box<dyn IComponent>>,
    pub z_index: u8,
}

impl IComponent for Window {
    fn add_child(&mut self, component: Component) {
        self.children.push(component);
    }

    fn set_position(&mut self, value: Vec2) {
        self.config.pos = value;
    }
    fn set_size(&mut self, value: Vec2) {
        self.config.size = value;
    }
    fn set_opacity(&mut self, value: f32) {
        self.config.opacity = value;
    }
    fn set_visible(&mut self, visible: bool) {
        self.config.visible = visible;
    }

    fn is_visible(&self) -> bool {
        return self.config.visible;
    }

    fn update(&mut self, _ui_root: &mut Ui) {
        // Do unique udpate stuff here
    }
}

pub struct Rectangle {
    pub skin: Skin,
    pub config: UIConfig,
    pub colour: Color,
}

impl IComponent for Rectangle {
    fn get_opacity(&self) -> f32 {
        self.config.opacity
    }

    fn set_position(&mut self, value: Vec2) {
        self.config.pos = value;
    }
    fn set_size(&mut self, value: Vec2) {
        self.config.size = value;
    }
    fn set_opacity(&mut self, value: f32) {
        self.config.opacity = value;
    }
    fn set_visible(&mut self, visible: bool) {
        self.config.visible = visible;
    }
    fn set_colour(&mut self, value: Color) {
        self.colour = value;
    }

    fn is_visible(&self) -> bool {
        return self.config.visible;
    }

    fn update(&mut self, ui_root: &mut Ui) {
        self.skin = {
            let colour = self.colour;
            let window_style = ui_root
                .style_builder()
                .color(Color::new(
                    colour.r,
                    colour.g,
                    colour.b,
                    self.config.opacity,
                ))
                .build();

            Skin {
                window_style,
                ..ui_root.default_skin()
            }
        };

        ui_root.push_skin(&self.skin);
        ui_root.window(hash!(), self.config.pos, self.config.size, |_| {});
        ui_root.pop_skin();
    }
}

#[derive(Debug, Clone)]
pub struct UIConfig {
    pub pos: Vec2,
    pub size: Vec2,
    pub opacity: f32,
    pub uri: Option<&'static [u8]>,
    pub visible: bool,
}

pub struct ButtonStatesConfig {
    pub hover_uri: &'static [u8],
    pub mouse_down_uri: &'static [u8],
    pub mouse_up: &'static [u8],
}

pub struct TextConfig {
    pub initial_text: String,
    pub initial_text_color: Color,
    pub font_size: u16,
    pub font_uri: &'static [u8],
}

pub struct Button {
    pub text_config: TextConfig,
    pub button_skin: Option<Skin>,
    pub config: UIConfig,
    pub state_uris: ButtonStatesConfig,
    pub is_clicked: bool,
}

impl IComponent for Button {
    fn get_position(&self) -> Vec2 {
        self.config.pos
    }

    fn get_opacity(&self) -> f32 {
        self.config.opacity
    }

    fn set_position(&mut self, value: Vec2) {
        self.config.pos = value;
    }
    fn set_size(&mut self, value: Vec2) {
        self.config.size = value;
    }
    fn set_opacity(&mut self, value: f32) {
        self.config.opacity = value;
    }
    fn set_visible(&mut self, visible: bool) {
        self.config.visible = visible;
    }

    fn is_clicked(&self) -> bool {
        return self.is_clicked;
    }

    fn is_visible(&self) -> bool {
        return self.config.visible;
    }

    fn update(&mut self, ui_root: &mut Ui) {
        if let Some(skin) = &self.button_skin {
            ui_root.push_skin(skin);

            if self.config.visible {
                if widgets::Button::new(&self.text_config.initial_text[..])
                    .size(self.config.size)
                    .position(self.config.pos)
                    .ui(ui_root)
                {
                    self.is_clicked = true;
                } else {
                    self.is_clicked = false;
                }
            } else {
                self.is_clicked = false;
            }

            ui_root.pop_skin();
        }
    }
}

impl Button {
    pub fn new(text_config: TextConfig, config: UIConfig, state_uris: ButtonStatesConfig) -> Self {
        let button_skin: Skin = {
            let button_style = root_ui()
                .style_builder()
                .font(text_config.font_uri)
                .unwrap()
                .text_color(Color::new(
                    text_config.initial_text_color.r,
                    text_config.initial_text_color.g,
                    text_config.initial_text_color.b,
                    1.0,
                ))
                .font_size(text_config.font_size)
                .background(Image::from_file_with_format(state_uris.mouse_up, None))
                .background_hovered(Image::from_file_with_format(state_uris.hover_uri, None))
                .background_clicked(Image::from_file_with_format(
                    state_uris.mouse_down_uri,
                    None,
                ))
                .color(Color::new(1., 1., 1., config.opacity))
                .build();

            Skin {
                button_style,
                ..root_ui().default_skin()
            }
        };

        let button = Button {
            text_config,
            button_skin: Some(button_skin),
            config,
            state_uris,
            is_clicked: false,
        };

        button
    }
}

pub struct StaticImage {
    pub config: UIConfig,
}

impl IComponent for StaticImage {
    fn get_position(&self) -> Vec2 {
        self.config.pos
    }

    fn set_position(&mut self, value: Vec2) {
        self.config.pos = value;
    }
    fn set_size(&mut self, value: Vec2) {
        self.config.size = value;
    }
    fn set_opacity(&mut self, value: f32) {
        self.config.opacity = value;
    }
    fn set_visible(&mut self, visible: bool) {
        self.config.visible = visible;
    }

    fn is_visible(&self) -> bool {
        return self.config.visible;
    }

    fn update(&mut self, ui_root: &mut Ui) {
        let texture2d = Texture2D::from_file_with_format(self.config.uri.unwrap(), None);
        widgets::Texture::new(texture2d)
            .size(self.config.size.x, self.config.size.y)
            .position(self.config.pos)
            .ui(ui_root);
    }
}

pub struct Checkbox {
    pub checkbox_skin: Skin,
    pub config: UIConfig,
    pub active: bool,
    pub text_config: TextConfig, // due to the label for the checkbox
}

impl IComponent for Checkbox {
    fn set_position(&mut self, value: Vec2) {
        self.config.pos = value;
    }
    fn set_size(&mut self, value: Vec2) {
        self.config.size = value;
    }
    fn set_opacity(&mut self, value: f32) {
        self.config.opacity = value;
    }
    fn set_visible(&mut self, visible: bool) {
        self.config.visible = visible;
    }

    fn is_visible(&self) -> bool {
        return self.config.visible;
    }
    fn update(&mut self, ui_root: &mut Ui) {
        ui_root.push_skin(&self.checkbox_skin);

        widgets::Group::new(hash!(), self.config.size)
            .position(self.config.pos)
            .ui(ui_root, |ui| {
                ui.checkbox(hash!(), &self.text_config.initial_text, &mut self.active);
            });

        ui_root.pop_skin();
    }
}

pub struct TextBlock {
    pub text_skin: Skin,
    pub config: UIConfig,
    pub text_config: TextConfig,
}

impl IComponent for TextBlock {
    fn set_position(&mut self, value: Vec2) {
        self.config.pos = value;
    }
    fn set_size(&mut self, value: Vec2) {
        self.config.size = value;
    }
    fn set_opacity(&mut self, value: f32) {
        self.config.opacity = value;
    }
    fn set_text(&mut self, value: String) {
        self.text_config.initial_text = value;
    }
    fn set_colour(&mut self, value: Color) {
        self.text_config.initial_text_color = value;
    }
    fn set_visible(&mut self, visible: bool) {
        self.config.visible = visible;
    }

    fn is_visible(&self) -> bool {
        return self.config.visible;
    }
    fn update(&mut self, ui_root: &mut Ui) {
        ui_root.push_skin(&self.text_skin);

        ui_root.label(self.config.pos, &self.text_config.initial_text);

        ui_root.pop_skin();
    }
}

impl TextBlock {
    pub fn new(config: UIConfig, text_config: TextConfig) -> Self {
        let text_skin: Skin = {
            let label_style = root_ui()
                .style_builder()
                .font(text_config.font_uri)
                .unwrap()
                .text_color(Color::new(
                    text_config.initial_text_color.r,
                    text_config.initial_text_color.g,
                    text_config.initial_text_color.b,
                    config.opacity,
                ))
                .font_size(text_config.font_size)
                .build();

            Skin {
                label_style,
                ..root_ui().default_skin()
            }
        };

        let textblock = TextBlock {
            text_skin,
            config,
            text_config,
        };

        textblock
    }
}

pub fn update() {
    let windows = get_ui_windows();
    let ui_root = &mut root_ui();

    windows.sort_by(|a, b| a.z_index.cmp(&b.z_index));

    windows.iter_mut().for_each(|window| {
        if window.is_visible() {
            let config = &window.config;
            let pos = config.pos;
            let size = config.size;

            ui_root.push_skin(&window.window_skin);
            ui_root.window(hash!(), pos, size, |ui| {
                window.children.iter_mut().for_each(|child| {
                    child.update(ui);
                });

                let screen_rect = {
                    let colour = Color::new(0., 0., 0., *get_screen_rect_opacity());
                    let button_style = ui
                        .style_builder()
                        .color(colour)
                        .color_hovered(colour)
                        .color_clicked(colour)
                        .color_selected(colour)
                        .color_selected_hovered(colour)
                        .color_inactive(colour)
                        .build();
                    Skin {
                        button_style,
                        ..ui.default_skin()
                    }
                };

                ui.push_skin(&screen_rect);

                widgets::Button::new("")
                    .size(Vec2::new(screen_width(), screen_height()))
                    .position(Vec2::new(0., 0.))
                    .ui(ui);

                ui.pop_skin();
            });

            ui_root.pop_skin();
        }
    });
}

pub mod ui_component_factory {
    use super::*;

    pub fn create_window(id: &'static str, config: UIConfig) {
        if get_ui_window(id).is_some() {
            return;
        }

        let window_skin: Skin = {
            let window_style = root_ui()
                .style_builder()
                .color(Color::new(0., 0., 0., config.opacity))
                .build();

            Skin {
                window_style,
                ..root_ui().default_skin()
            }
        };

        let window = Window {
            id,
            window_skin,
            config,
            z_index: 0,
            children: Vec::new(),
        };

        get_ui_windows().push(window);
    }

    pub fn create_rect(window_id: &'static str, config: UIConfig, colour: Color) {
        let skin: Skin = {
            let window_style = root_ui()
                .style_builder()
                .color(Color::new(colour.r, colour.g, colour.b, config.opacity))
                .build();

            Skin {
                window_style,
                ..root_ui().default_skin()
            }
        };

        let rect = Rectangle {
            skin,
            config,
            colour,
        };

        add_component(window_id, Box::new(rect));
    }

    pub fn create_group(window_id: &'static str, config: UIConfig, draggable: bool, group_index: Option<usize>) {
        let group_skin: Skin = {
            let group_style = root_ui()
                .style_builder()
                .color(Color::new(1., 1., 1., config.opacity))
                .build();

            Skin {
                group_style,
                ..root_ui().default_skin()
            }
        };

        let group_component = GroupComponent {
            group_skin,
            config,
            children: Vec::new(),
            draggable,
            colour: Color::new(1., 1., 1., 0.),
            group_index,
            drag_state: DraggingState::None,
            drag_position: Vec2::new(0., 0.),
        };

        add_component(window_id, Box::new(group_component));
    }

    pub fn create_checkbox(
        window_id: &'static str,
        config: UIConfig,
        text_config: TextConfig,
        state_uris: ButtonStatesConfig,
    ) {
        let checkbox_skin: Skin = {
            let label_style = root_ui()
                .style_builder()
                .font(text_config.font_uri)
                .unwrap()
                .text_color(text_config.initial_text_color)
                .font_size(text_config.font_size)
                .build();
            
            let checkbox_style = root_ui()
                .style_builder()
                .background(Image::from_file_with_format(state_uris.mouse_up, None))
                .background_hovered(Image::from_file_with_format(state_uris.hover_uri, None))
                .background_clicked(Image::from_file_with_format(
                    state_uris.mouse_down_uri,
                    None,
                ))
                .color(Color::new(1., 1., 1., config.opacity))
                .build();

            let group_style = root_ui()
                .style_builder()
                .color(Color::from_rgba(255, 255, 255, 0))
                .build();

            Skin {
                label_style,
                checkbox_style,
                group_style,
                ..root_ui().default_skin()
            }
        };

        let checkbox = Checkbox {
            checkbox_skin,
            config,
            active: false,
            text_config,
        };

        add_component(window_id, Box::new(checkbox));
    }

    pub fn create_static_image(window_id: &'static str, config: UIConfig) {
        let staticimage = StaticImage { config };
        add_component(window_id, Box::new(staticimage));
    }

    pub fn create_textblock(window_id: &'static str, config: UIConfig, text_config: TextConfig) {
        let text_skin: Skin = {
            let label_style = root_ui()
                .style_builder()
                .font(text_config.font_uri)
                .unwrap()
                .text_color(Color::new(
                    text_config.initial_text_color.r,
                    text_config.initial_text_color.g,
                    text_config.initial_text_color.b,
                    config.opacity,
                ))
                .font_size(text_config.font_size)
                .build();

            Skin {
                label_style,
                ..root_ui().default_skin()
            }
        };

        let textblock = TextBlock {
            text_skin,
            config,
            text_config,
        };

        add_component(window_id, Box::new(textblock));
    }

    pub fn create_button(
        window_id: &'static str,
        text_config: TextConfig,
        config: UIConfig,
        state_uris: ButtonStatesConfig,
    ) {
        let button = Button::new(text_config, config, state_uris);
        add_component(window_id, Box::new(button));
    }
}
